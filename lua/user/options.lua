vim.g.mapleader = " " -- maps which key should <leader> be
vim.g.tex_flavor = "latex"

vim.opt.conceallevel = 2 -- makes some stuff pretty by hiding it
vim.opt.guicursor = "" -- no cursor decorations
vim.opt.laststatus = 3
vim.opt.pumblend = 20 -- transparency
vim.opt.pumheight = 15
vim.opt.showmode = false -- already in statusbar so we don't need it
vim.opt.termguicolors = true

vim.opt.spelllang = { "en", "cs" }
vim.opt.updatetime = 200 -- 4000ms default
vim.opt.timeout = true
vim.opt.timeoutlen = 500 -- how long to wait for next keymap
vim.opt.completeopt = "menu,menuone,noinsert"
vim.opt.path:append "**" -- for "fuzzy" matching of files in cmdline
vim.opt.confirm = true

vim.opt.number = true -- enables numbers
vim.opt.relativenumber = true -- enables better numbers, cool for [count]motions
vim.opt.wrap = false -- don't wrap lines
vim.opt.linebreak = true -- wraps lines smartly
vim.opt.cursorline = true
vim.opt.signcolumn = "yes"
vim.opt.colorcolumn = "100"

vim.opt.mouse = "a" -- enables mouse
vim.opt.scrolloff = 8
vim.opt.sidescrolloff = 8

if vim.fn.has "nvim-0.10" == 1 then
	vim.opt.smoothscroll = true
end

-- Indentation
vim.opt.autoindent = true
vim.opt.cindent = true -- makes intention work better for c like languages :h 'cindent'
vim.opt.expandtab = true -- makes tab into spaces
vim.opt.shiftround = true
vim.opt.shiftwidth = 4
vim.opt.softtabstop = 4
vim.opt.tabstop = 4

vim.opt.incsearch = true
vim.opt.ignorecase = true
vim.opt.smartcase = true -- search won't care about uppercase until you use a uppercase characters

-- Makes split splitting bit more humane
vim.opt.splitright = true
vim.opt.splitbelow = true
vim.opt.winminwidth = 5

vim.opt.fileencoding = "utf-8"
vim.opt.swapfile = false -- gets annoying but you can lose your files if you don't save them
vim.opt.hidden = true
vim.opt.undofile = true -- best thing, basically git for normal files try <leader>u for undotree
vim.opt.undolevels = 1000
vim.opt.undoreload = 10000

-- Folds
vim.opt.foldexpr = "v:lua.vim.treesitter.foldexpr()"
vim.opt.foldmethod = "expr"
vim.opt.foldtext = ""
vim.opt.foldlevelstart = 99

vim.opt.fillchars:append {
	foldopen = "-",
	foldclose = "+",
	foldsep = " ",
}

-- Faster grep, written in rust btw
if vim.fn.executable "rg" == 1 then
	vim.opt.grepprg = "rg --vimgrep"
end

-- Highlights some special characters
vim.opt.list = true
vim.opt.listchars:append {
	eol = "↲",
	tab = "» ",
	space = " ",
	trail = "·",
	extends = "<",
	precedes = ">",
	conceal = "┊",
	nbsp = "␣",
}

-- :h 'shortmess' for more
vim.opt.shortmess:append {
	I = true, -- disables the default intro, can be bit buggy with lazy loading
	c = true, -- disables completion messages
}
