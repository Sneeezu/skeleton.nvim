vim.diagnostic.config {
	virtual_text = true,
	underline = true,
	update_in_insert = true,
	severity_sort = true,

	float = {
		focusable = true,
		source = "if_many",
	},

	signs = {
		text = {
			[vim.diagnostic.severity.ERROR] = "E",
			[vim.diagnostic.severity.WARN] = "W",
			[vim.diagnostic.severity.HINT] = "H",
			[vim.diagnostic.severity.INFO] = "I",
		},
	},
}
