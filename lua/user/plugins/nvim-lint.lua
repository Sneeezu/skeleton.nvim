return {
	"mfussenegger/nvim-lint",
	event = { "BufReadPost", "BufNewFile", "VeryLazy" },

	config = function()
		require("lint").linters_by_ft = {
			go = { "revive" },
		}

		vim.api.nvim_create_autocmd({
			"BufWritePost",
			"BufReadPost",
			"InsertLeave",
			"TextChanged",
		}, {
			callback = function()
				require("lint").try_lint()
			end,
		})
	end,
}
