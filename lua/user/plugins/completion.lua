return {
	{
		"L3MON4D3/LuaSnip",
		lazy = true,

		build = "make install_jsregexp",
		enabled = vim.fn.executable "make" == 1,

		dependencies = {
			{
				"rafamadriz/friendly-snippets",
				config = function()
					require("luasnip.loaders.from_vscode").lazy_load()
				end,
			},
		},

		opts = {
			history = true,
			enable_autosnippets = true,
			updateevents = { "TextChanged", "TextChangedI" },
		},

		config = function(_, opts)
			require("luasnip").config.setup(opts)
		end,
	},

	{
		"hrsh7th/nvim-cmp",
		event = "InsertEnter",

		dependencies = {
			"hrsh7th/cmp-path",
			"hrsh7th/cmp-buffer",
			"hrsh7th/cmp-nvim-lsp",
			"f3fora/cmp-spell",

			{
				"saadparwaiz1/cmp_luasnip",
				dependencies = {
					"l3MON4D3/LuaSnip",
				},
			},
		},

		config = function()
			local cmp = require "cmp"
			local ls = require "luasnip"

			local kind_icons = {
				Class = "󰌗 ",
				Color = "󰏘 ",
				Constant = "󰏿 ",
				Constructor = " ",
				Enum = " ",
				EnumMember = " ",
				Event = " ",
				Field = "󰜢 ",
				File = "󰈙 ",
				Folder = " ",
				Function = "󰊕 ",
				Interface = " ",
				Keyword = "󰌋 ",
				Method = "󰆧 ",
				Module = " ",
				Operator = "󰆕 ",
				Property = " ",
				Reference = " ",
				Snippet = "󱥒 ",
				Struct = "󰙅 ",
				Text = "󰉿 ",
				TypeParameter = "󰊄 ",
				Unit = " ",
				Value = "󰎠 ",
				Variable = "󰀫 ",
			}

			local source_mapping = {
				nvim_lsp = "[LSP]",
				luasnip = "[Snip]",
				path = "[Path]",
				buffer = "[Buff]",
				spell = "[Spell]",
			}

			cmp.setup {
				preselect = cmp.PreselectMode.None,

				completion = {
					completeopt = "menu,menuone,noinsert",
				},

				snippet = {
					expand = function(args)
						ls.lsp_expand(args.body)
					end,
				},

				-- window = {
				-- 	completion = {
				-- 		scrolloff = 3,
				-- 		winhighlight = "Normal:NormalFloat,FloatBorder:NormalFloat,CursorLine:Visual,Search:None",
				-- 	},

				-- 	documentation = {
				-- 		winhighlight = "Normal:NormalFloat,FloatBorder:NormalFloat,CursorLine:Visual,Search:None",
				-- 	},
				-- },

				formatting = {
					expandable_indicator = true,

					fields = { "abbr", "kind", "menu" },

					format = function(entry, item)
						item.menu = source_mapping[entry.source.name]
						item.kind = kind_icons[item.kind] .. item.kind

						local content = item.abbr
						local win_width = vim.api.nvim_win_get_width(0)
						local max_content_width = math.floor(win_width * 0.20)

						if #content > max_content_width then
							item.abbr = vim.fn.strcharpart(content, 0, max_content_width - 1) .. "…"
						else
							item.abbr = content .. (" "):rep(max_content_width - #content)
						end

						return item
					end,
				},

				sources = cmp.config.sources({
					{ name = "nvim_lsp" },
					{ name = "luasnip" },
				}, {
					{ name = "path" },
					{ name = "buffer", keyword_length = 4 },
					{
						name = "spell",
						option = {
							keep_all_entries = false,
							enable_in_context = function()
								return vim.opt.spell
							end,
						},
					},
				}),

				mapping = cmp.mapping.preset.insert {
					["<C-u>"] = cmp.mapping(cmp.mapping.scroll_docs(-4), { "i", "c" }),
					["<C-d>"] = cmp.mapping(cmp.mapping.scroll_docs(4), { "i", "c" }),
					["<C-Space>"] = cmp.mapping(cmp.mapping.complete(), { "i", "c" }),

					["<C-p>"] = cmp.mapping(function()
						if cmp.visible() then
							cmp.select_prev_item { behavior = "insert" }
						else
							cmp.complete()
						end
					end),

					["<C-n>"] = cmp.mapping(function()
						if cmp.visible() then
							cmp.select_next_item { behavior = "insert" }
						else
							cmp.complete()
						end
					end),

					["<C-y>"] = cmp.mapping.confirm {
						behavior = cmp.ConfirmBehavior.insert,
						select = true,
					},

					["<C-e>"] = cmp.mapping {
						i = cmp.mapping.abort(),
						c = cmp.mapping.close(),
					},

					["<C-j>"] = cmp.mapping(function()
						if ls.jumpable(1) then
							ls.jump(1)
						end
					end, { "s", "i" }),

					["<C-k>"] = cmp.mapping(function()
						if ls.jumpable(-1) then
							ls.jump(-1)
						end
					end, { "s", "i" }),

					["<C-l>"] = cmp.mapping(function()
						if require("luasnip").choice_active() then
							require("luasnip").change_choice(1)
						end
					end, { "s", "i" }),
				},
			}
		end,
	},
}
