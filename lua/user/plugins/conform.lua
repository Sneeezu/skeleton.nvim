return {
	"stevearc/conform.nvim",
	event = { "BufWritePre" },

	cmd = { "ConformInfo" },

	init = function()
		-- HACK: fixes https://github.com/neovim/neovim/issues/21856
		vim.api.nvim_create_autocmd("VimLeave", {
			callback = function()
				vim.fn.jobstart("", {
					detach = true,
				})
			end,
		})

		vim.api.nvim_create_user_command("FormatDisable", function(args)
			if args.bang then
				vim.b.disable_format_on_save = true
			else
				vim.g.disable_format_on_save = true
			end
		end, {
			desc = "Disable format on save",
			bang = true,
		})

		vim.api.nvim_create_user_command("FormatEnable", function()
			vim.b.disable_format_on_save = false
			vim.g.disable_format_on_save = false
		end, {
			desc = "Enable format on save",
		})

		vim.o.formatexpr = "v:lua.require'conform'.formatexpr()"
	end,

	keys = {
		{
			"<leader>f",
			function()
				require("conform").format {
					async = true,
					lsp_fallback = true,
				}
			end,
			desc = "Format buffer",
		},
	},

	opts = {
		formatters_by_ft = {
			sh = { "shfmt" },
			bash = { "shfmt" },

			lua = { "stylua" },
			lau = { "stylua" },

			rust = { "rustfmt" },

			tex = { "trim_whitespace" },
			css = { "prettier" },
			html = { "prettier" },
			markdown = { "prettier" },
			javascript = { "prettier" },
			typescript = { "prettier" },
		},

		format_on_save = function(bufnr)
			if vim.g.disable_format_on_save or vim.b[bufnr].disable_format_on_save then
				return
			end

			return {
				timeout_ms = 500,
				lsp_fallback = true,
			}
		end,

		formatters = {
			shfmt = {
				prepend_args = { "-i", "4" },
			},

			prettier = {
				prepend_args = { "--tab-width", "4" },
			},
		},
	},
}
