return {
	"catppuccin/nvim",
	priority = 1000,
	lazy = false,
	name = "catppuccin",

	config = function()
		require("catppuccin").setup {
			flavour = "mocha",
			show_end_of_buffer = true,

			background = {
				light = "latte",
				dark = "mocha",
			},

			styles = {
				comments = { "italic" },
				functions = { "bold" },
			},

			integrations = {
				cmp = true,
				fidget = true,
				gitsigns = true,
				harpoon = true,
				mason = true,
				mini = true,
				native_lsp = {
					enabled = true,
					virtual_text = {
						errors = {},
						hints = {},
						warnings = {},
						information = {},
					},
					underlines = {
						errors = { "undercurl" },
						hints = { "undercurl" },
						warnings = { "undercurl" },
						information = { "undercurl" },
					},
					inlay_hints = {
						background = true,
					},
				},
				neotest = true,
				semantic_tokens = true,
				treesitter = true,
				treesitter_context = true,
				which_key = true,
			},

			custom_highlights = function(colors)
				return {
					-- Diagnostics
					DiagnosticVirtualTextHint = { bg = colors.none },
					DiagnosticVirtualTextInfo = { bg = colors.none },
					DiagnosticVirtualTextWarn = { bg = colors.none },
					DiagnosticVirtualTextError = { bg = colors.none },

					-- Plugins
					LazySpecial = { fg = colors.blue },
					TelescopeMatching = { fg = colors.red },
					TreesitterContextBottom = { style = {} },
					TreesitterContextLineNumber = { fg = colors.surface1, bg = colors.mantle },
					WhichKey = { fg = colors.flamingo },
					WhichKeyBorder = { fg = colors.blue },

					-- Default higlights
					CursorLineNr = { bold = true },
					Folded = { fg = colors.subtext0, bg = colors.mantle },
					Pmenu = { bg = colors.mantle },
					TabLine = { fg = colors.overlay1, bg = colors.mantle },
					TabLineFill = { bg = colors.mantle },
					TabLineSel = { fg = colors.text, bg = colors.surface0 },
					WinSeparator = { fg = colors.mantle },
				}
			end,
		}

		vim.cmd.colorscheme "catppuccin"
	end,
}
