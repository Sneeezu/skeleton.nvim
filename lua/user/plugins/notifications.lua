return {
	"j-hui/fidget.nvim",
	event = "VeryLazy",

	opts = {
		progress = {
			display = {
				done_icon = " ",
				progress_icon = {
					pattern = {
						"_ ",
						"_ ",
						"_ ",
						"- ",
						"` ",
						"` ",
						"' ",
						"´ ",
						"- ",
						"_ ",
						"_ ",
						"_ ",
					},
				},
			},

			lsp = {
				progress_ringbuf_size = 10000,
			},
		},

		notification = {
			override_vim_notify = true,

			view = {
				stack_upwards = false,
				group_separator = "",
				group_separator_hl = "Comment",
			},

			window = {
				border = "rounded",
				x_padding = 0,
				y_padding = 0,
			},
		},
	},
}
