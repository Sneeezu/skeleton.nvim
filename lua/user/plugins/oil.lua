return {
	"stevearc/oil.nvim",
	lazy = false,

	keys = {
		{ "<leader>t", "<cmd>Oil<CR>", desc = "Open Oil" },
	},

	opts = {
		default_file_explorer = true,

		columns = {
			"permissions",
			"size",
			"mtime",
		},

		use_default_keymaps = false,
		keymaps = {
			["g?"] = "actions.show_help",
			["<CR>"] = "actions.select",
			["<A-v>"] = "actions.select_vsplit",
			["<C-s>"] = "actions.select_split",
			["<C-t>"] = "actions.select_tab",
			["<C-c>"] = "actions.close",
			["<C-l>"] = "actions.refresh",
			["-"] = "actions.parent",
			["_"] = "actions.open_cwd",
			["cd"] = "actions.cd",
			["<leader>cd"] = "actions.tcd",
			["gs"] = "actions.change_sort",
			["gx"] = "actions.open_external",
			["g."] = "actions.toggle_hidden",
			["g\\"] = "actions.toggle_trash",
		},

		view_options = {
			show_hidden = true,

			is_always_hidden = function(name, _)
				return name == ".."
			end,
		},
	},
}
