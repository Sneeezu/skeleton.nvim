return {
	"echasnovski/mini.statusline",
	commit = "bf6158d",

	opts = {
		use_icons = true,
		set_vim_settings = false,
	},

	config = function(_, opts)
		require("mini.statusline").setup(opts)
	end,
}
