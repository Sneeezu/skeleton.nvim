---@alias Packages (string|PackageConfig)[]

---@class PackageConfig
---@field [1] string
---@field cond? boolean

---Function to install single package
---@param pkg_name string
---@param on_success? function():nil
---@param on_failiure? function():nil
local function install_package(pkg_name, on_success, on_failiure)
	local ok, pkg = pcall(require("mason-registry").get_package, pkg_name)
	if ok == false then
		vim.notify(string.format('Package "%s" is not a valid.', pkg_name), vim.log.levels.WARN, {
			title = "mason.nvim",
		})
		return
	end

	if pkg:is_installed() then
		return
	end

	vim.notify(string.format('Installing package "%s".', pkg_name), vim.log.levels.INFO, {
		title = "mason.nvim",
	})

	pkg:install():once(
		"closed",
		vim.schedule_wrap(function()
			if pkg:is_installed() == false then
				vim.notify(
					string.format('Package "%s" failed to install. See for more info see mason logs.', pkg_name),
					vim.log.levels.ERROR,
					{
						title = "mason.nvim",
					}
				)

				if type(on_failiure) == "function" then
					on_failiure()
				end
			else
				vim.notify(string.format('Package "%s" was successfully installed.', pkg_name), vim.log.levels.INFO, {
					title = "mason.nvim",
				})

				if type(on_success) == "function" then
					on_success()
				end
			end
		end)
	)
end

---Function to install all of the packages
---@param packages Packages
---@param on_success? function():nil
---@param on_failiure? function():nil
local function ensure_installed(packages, on_success, on_failiure)
	for _, cfg in ipairs(packages) do
		---@type string
		local pkg_name

		if type(cfg) == "string" then
			pkg_name = cfg
		else
			if cfg.cond == false then
				goto continue
			end
			pkg_name = cfg[1]
		end

		install_package(pkg_name, on_success, on_failiure)

		::continue::
	end
end

return {
	"williamboman/mason.nvim",
	cmd = "Mason",
	build = ":MasonUpdate",

	keys = {
		{ "<leader>m", "<cmd>Mason<CR>", desc = "Mason" },
	},

	opts = {
		---@type Packages
		ensure_installed = {
			-- Lsps
			"bash-language-server",
			"clangd",
			"css-lsp",
			{ "gopls", cond = vim.fn.executable "go" == 1 },
			-- "haskell-language-server", -- install through ghcup, it does better job
			"html-lsp",
			"json-lsp",
			"lua-language-server",
			"marksman",
			"basedpyright",
			"ruff-lsp",
			"rust-analyzer",
			"taplo",
			"texlab",
			"typescript-language-server",
			"typst-lsp",

			-- Linters
			"shellcheck",
			{ "revive", cond = vim.fn.executable "go" == 1 },

			-- Formatters
			"prettier",
			-- "rustfmt", -- deprecated, install through rustup
			"shfmt",
			"stylua",
		},

		ui = {
			border = "rounded",
			width = 0.9,
			height = 0.85,

			check_outdated_packages_on_open = true,
			icons = {
				package_installed = "✓",
				package_pending = "➜",
				package_uninstalled = "✗",
			},
		},
	},

	config = function(_, opts)
		require("mason").setup(opts)

		if opts.ensure_installed == nil and next(opts.ensure_installed) == nil then
			return
		end

		ensure_installed(opts.ensure_installed, function()
			vim.defer_fn(function()
				-- Try setting up lsp by triggering the FileType event
				require("lazy.core.handler.event").trigger {
					event = "FileType",
					buf = vim.api.nvim_get_current_buf(),
				}

				-- Try linting the current file
				local ok, lint = pcall(require, "lint")
				if ok == true then
					lint.try_lint()
				end
			end, 100)
		end)
	end,
}
