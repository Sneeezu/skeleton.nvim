return {
	"nvim-neotest/neotest",
	cmd = "Neotest",

	keys = {
		{
			"<leader>xr",
			function()
				require("neotest").run.run()
			end,
			desc = "Run test",
		},
		{
			"<leader>xR",
			function()
				require("neotest").run.run(vim.fn.expand "%")
			end,
			desc = "Run all test in a file",
		},
		{
			"<leader>xs",
			function()
				require("neotest").summary.toggle()
			end,
			desc = "Open neotest summary",
		},
	},

	dependencies = {
		"nvim-neotest/nvim-nio",
		"nvim-lua/plenary.nvim",
		"antoinemadec/FixCursorHold.nvim",
		"nvim-treesitter/nvim-treesitter",

		"nvim-neotest/neotest-python",
	},

	config = function()
		require("neotest").setup {
			adapters = {
				require "neotest-python",
			},
			icons = {
				failed = "F",
				notify = "N",
				passed = "P",
				running = "R",
				skipped = "S",
				unknown = "?",
				watching = "W",
			},
		}
	end,
}
