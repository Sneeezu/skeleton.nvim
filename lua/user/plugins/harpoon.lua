return {
	"ThePrimeagen/harpoon",

	keys = {
		{
			"<leader>a",
			function()
				require("harpoon.mark").add_file()
			end,
			desc = "Add file to harpoon",
		},
		{
			"<leader>h",
			function()
				require("harpoon.ui").toggle_quick_menu()
			end,
			desc = "Open harpoon quick menu",
		},
		{
			"<C-h>",
			function()
				require("harpoon.ui").nav_file(1)
			end,
			desc = "Navigate to first harpoon file",
		},
		{
			"<C-t>",
			function()
				require("harpoon.ui").nav_file(2)
			end,
			desc = "Navigate to second harpoon file",
		},
		{
			"<C-n>",
			function()
				require("harpoon.ui").nav_file(3)
			end,
			desc = "Navigate to third harpoon file",
		},
		{
			"<C-s>",
			function()
				require("harpoon.ui").nav_file(4)
			end,
			desc = "Navigate to fourth harpoon file",
		},
	},

	dependencies = {
		"nvim-lua/plenary.nvim",
	},
}
