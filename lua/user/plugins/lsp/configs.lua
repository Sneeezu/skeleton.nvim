-- lsp configs
-- NOTE: make sure to install or add the lsp server to ensure_installed table in masons
-- config also don't forget to uncomment or add it to this config table
return {
	-- Lua
	lua_ls = {
		settings = {
			Lua = {
				runtime = {
					version = "LuaJIT",
					path = vim.split(package.path, ";"),
				},

				completion = {
					callSnippet = "Replace",
					autoRequire = false,
				},

				workspace = {
					checkThirdParty = false,
				},

				telemetry = {
					enable = false,
				},

				hint = {
					enable = true,
				},
			},
		},
	},

	-- Golang
	gopls = {
		custom_on_attach = function(_, buffer)
			vim.api.nvim_create_autocmd("BufWritePre", {
				group = vim.api.nvim_create_augroup("GoSortImports" .. buffer, { clear = true }),
				buffer = buffer,
				callback = function()
					local params = vim.lsp.util.make_range_params()
					params.context = { only = { "source.organizeImports" } }

					-- buf_request_sync defaults to a 1000ms timeout. Depending on your
					-- machine and codebase, you may want longer. Add an additional
					-- argument after params if you find that you have to write the file
					-- twice for changes to be saved.
					-- E.g., vim.lsp.buf_request_sync(0, "textDocument/codeAction", params, 3000)
					local result = vim.lsp.buf_request_sync(0, "textDocument/codeAction", params)
					for cid, res in pairs(result or {}) do
						for _, r in pairs(res.result or {}) do
							if r.edit then
								local enc = (vim.lsp.get_client_by_id(cid) or {}).offset_encoding or "utf-8"
								vim.lsp.util.apply_workspace_edit(r.edit, enc)
							end
						end
					end
				end,
			})
		end,

		settings = {
			gopls = {
				staticcheck = true,
				usePlaceholders = true,
				symbolStyle = "Full",

				hints = {
					assignVariableTypes = true,
					compositeLiteralFields = true,
					compositeLiteralTypes = true,
					constantValues = true,
					functionTypeParameters = true,
					parameterNames = true,
					rangeVariableTypes = true,
				},

				analyses = {
					unusedparams = true,
				},
			},
		},
	},

	-- Rust
	rust_analyzer = {
		settings = {
			["rust-analyzer"] = {
				check = {
					command = "clippy",
				},
			},
		},
	},

	-- JS and TS
	tsserver = {
		settings = {
			typescript = {
				inlayHints = {
					includeInlayParameterNameHints = "all",
					includeInlayParameterNameHintsWhenArgumentMatchesName = false,
					includeInlayFunctionParameterTypeHints = true,
					includeInlayVariableTypeHints = true,
					includeInlayVariableTypeHintsWhenTypeMatchesName = false,
					includeInlayPropertyDeclarationTypeHints = true,
					includeInlayFunctionLikeReturnTypeHints = true,
					includeInlayEnumMemberValueHints = true,
				},
			},

			javascript = {
				inlayHints = {
					includeInlayParameterNameHints = "all",
					includeInlayParameterNameHintsWhenArgumentMatchesName = false,
					includeInlayFunctionParameterTypeHints = true,
					includeInlayVariableTypeHints = true,
					includeInlayVariableTypeHintsWhenTypeMatchesName = false,
					includeInlayPropertyDeclarationTypeHints = true,
					includeInlayFunctionLikeReturnTypeHints = true,
					includeInlayEnumMemberValueHints = true,
				},
			},
		},
	},

	-- Python
	ruff_lsp = {
		custom_on_attach = function(client, _)
			client.server_capabilities.hoverProvider = false
		end,
	},

	basedpyright = {
		settings = {
			basedpyright = {
				analysis = {
					autoSearchPaths = true,
					diagnosticMode = "workspace",
					useLibraryCodeForTypes = true,

					diagnosticSeverityOverrides = {
						-- reportImplicitOverride = false,
						reportMissingSuperCall = false,
						reportMissingTypeStubs = false,
					},
				},
			},
		},
	},

	-- Latex - tectonic
	texlab = {
		settings = {
			texlab = {
				build = {
					onSave = true,
					executable = "tectonic",
					auxDirectory = "target",
					pdfDirectory = "target",
					args = {
						"-X",
						"compile",
						"%f",
						"--outdir=target",
						"--synctex",
						"--keep-logs",
						"--keep-intermediates",
					},
				},
			},
		},
	},

	-- Better latex (typst)
	typst_lsp = {
		settings = {
			exportPdf = "onSave", -- or onType, but some pdf viewers have problem with this setting
		},
	},

	-- C and C++
	clangd = {},

	-- Bash
	bashls = {},

	-- Haskell
	hls = {},

	-- Web stuff (html, css)
	html = {},
	cssls = {},

	-- Json
	jsonls = {},

	-- Toml
	taplo = {},

	-- Markdown
	marksman = {},
}
