-- HACK: https://github.com/hrsh7th/nvim-cmp/issues/1699#issuecomment-1738132283
local M = {}

local methods = vim.lsp.protocol.Methods
local md_namespace = vim.api.nvim_create_namespace "lsp_float"

--- Adds extra inline highlights to the given buffer
---@param buf integer
local function add_inline_highlights(buf)
	local patterns = {
		["@%S+"] = "@variable.parameter",
		["^%s*(Parameters:)"] = "@text.title",
		["^%s*(Return:)"] = "@text.title",
		["^%s*(See also:)"] = "@text.title",
		['"%s-"'] = "String",
		["'%s-'"] = "String",
		["•%s*{%S-}"] = "@variable.parameter",
		["•%s*"] = "@text.reference",
		["^[─]+$"] = "@symbol",
		-- ["|%S-|"] = "@text.reference",
	}

	for l, line in ipairs(vim.api.nvim_buf_get_lines(buf, 0, -1, false)) do
		for pattern, hl_group in pairs(patterns) do
			---@type integer?
			local from = 1

			while from do
				local to
				from, to = line:find(pattern, from)
				if from then
					vim.api.nvim_buf_set_extmark(buf, md_namespace, l - 1, from - 1, {
						end_col = to,
						hl_group = hl_group,
					})
				end
				from = to and to + 1 or nil
			end
		end
	end
end

--- LSP handler that adds extra inline highlights, keymaps, and window options
---@param handler fun(err: any, result: any, ctx: any, config: any): integer?, integer?
---@return fun(err: any, result: any, ctx: any, config: any)
local function enhanced_float_handler(handler)
	return function(err, result, ctx, config)
		local bufnr, winnr = handler(err, result, ctx, config)
		if not bufnr or not winnr then
			return
		end

		-- conceal everything
		vim.wo[winnr].concealcursor = "n"

		if vim.b[bufnr].markdown_keys then
			return
		end

		-- add keymaps for opening links
		vim.keymap.set("n", "gx", function()
			-- vim help links
			local url = (vim.fn.expand "<cWORD>" --[[@as string]]):match "|(%S-)|"
			if url then
				return vim.cmd.help(url)
			end

			-- markdown links
			local col = vim.api.nvim_win_get_cursor(0)[2] + 1
			local from, to
			from, to, url = vim.api.nvim_get_current_line():find "%[.-%]%((%S-)%)"

			if from and col >= from and col <= to then
				local code = vim.ui.open(url)
				if code == nil then
					vim.notify("Failed to open url: " .. url, vim.log.levels.ERROR)
				end
			end
		end, { buffer = bufnr, silent = true })

		vim.b[bufnr].markdown_keys = true

		add_inline_highlights(bufnr)
	end
end

--- HACK: overrides `vim.lsp.util.stylize_markdown` to use Treesitter.
---@param bufnr integer
---@param contents string[]
---@param opts table
---@return string[]
---@diagnostic disable-next-line: duplicate-set-field
local function stylize_markdown(bufnr, contents, opts)
	contents = vim.lsp.util._normalize_markdown(contents, {
		width = vim.lsp.util._make_floating_popup_size(contents, opts),
	})

	vim.bo[bufnr].filetype = "markdown"
	vim.treesitter.start(bufnr)
	vim.api.nvim_buf_set_lines(bufnr, 0, -1, false, contents)

	add_inline_highlights(bufnr)

	return contents
end

M.setup = function(config)
	if config.stylize_markdown == true then
		vim.lsp.util.stylize_markdown = stylize_markdown
	end

	if config.textDocument_hover == true then
		vim.lsp.handlers[methods.textDocument_hover] = enhanced_float_handler(vim.lsp.handlers.hover)
	end

	if config.textDocument_signatureHelp == true then
		vim.lsp.handlers[methods.textDocument_signatureHelp] = enhanced_float_handler(vim.lsp.handlers.signature_help)
	end
end

return M
