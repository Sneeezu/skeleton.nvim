local function inlay_hints_setup(client, buffer)
	if client.supports_method "textDocument/inlayHint" then
		if pcall(vim.lsp.inlay_hint.enable, true, { bufnr = buffer }) then
			return
		end

		if pcall(vim.lsp.inlay_hint.enable, buffer, true) then
			return
		end

		vim.notify("Failed setting up inlay hints, try updating your Neovim.", vim.log.levels.WARN, {
			title = "Inlay Hints",
		})
	end
end

local function on_attach(client, buffer)
	local function map(mode, l, r, desc)
		vim.keymap.set(mode, l, r, { buffer = buffer, remap = false, desc = desc })
	end

	map("n", "gd", vim.lsp.buf.definition, "Go to definition(s)")
	map("n", "gr", vim.lsp.buf.references, "Go to reference(s)")
	map("n", "gi", vim.lsp.buf.implementation, "Go to implementation(s)")
	map("n", "gD", vim.lsp.buf.declaration, "Go to declaration")
	map("n", "<leader>D", vim.lsp.buf.type_definition, "Go to type definition")

	map("n", "K", vim.lsp.buf.hover, "Show documentation")
	map("n", "<leader>cr", vim.lsp.buf.rename, "Rename")
	map({ "n", "v" }, "<leader>ca", vim.lsp.buf.code_action, "Code action")
	map({ "i", "s" }, "<C-s>", vim.lsp.buf.signature_help, "Signature help")

	map("n", "<leader>wa", vim.lsp.buf.add_workspace_folder)
	map("n", "<leader>wr", vim.lsp.buf.remove_workspace_folder)
	map("n", "<leader>wl", function()
		print(vim.inspect(vim.lsp.buf.list_workleader_folders()))
	end)

	inlay_hints_setup(client, buffer)

	require("user.plugins.lsp.markdown").setup {
		stylize_markdown = true,
		textDocument_hover = true,
		textDocument_signatureHelp = true,
	}
end

return {
	"neovim/nvim-lspconfig",
	event = { "BufReadPost", "BufNewFile" },

	keys = {
		{ "<leader>li", "<cmd>LspInfo<CR>" },
		{ "<leader>lr", "<cmd>LspRestart<CR>" },
	},

	dependencies = {
		"williamboman/mason.nvim",
		"hrsh7th/cmp-nvim-lsp",

		-- makes the lua_ls server to know where neovim plugins are and stuff, for better completion
		{
			"folke/neodev.nvim",
			opts = {
				experimental = {
					pathStrict = true,
				},
			},
		},
	},

	config = function()
		local lspconfig = require "lspconfig"

		require("lspconfig.ui.windows").default_options = {
			border = "rounded",
		}

		local capabilities = vim.tbl_deep_extend(
			"force",
			vim.lsp.protocol.make_client_capabilities(),
			require("cmp_nvim_lsp").default_capabilities()
		)

		for server, config in pairs(require "user.plugins.lsp.configs") do
			config.capabilities = capabilities

			config.on_attach = function(client, buffer)
				if type(config.custom_on_attach) == "function" then
					config.custom_on_attach(client, buffer)
				end

				on_attach(client, buffer)
			end

			lspconfig[server].setup(config)
		end
	end,
}
