---@param name string
---@param fn fun(name:string)
local function on_load(name, fn)
	local cfg = require "lazy.core.config"
	if cfg.plugins[name] and cfg.plugins[name]._.loaded then
		fn(name)
		return
	end

	vim.api.nvim_create_autocmd("User", {
		pattern = "LazyLoad",
		callback = function(event)
			if event.data == name then
				fn(name)
				return true
			end
		end,
	})
end

return {
	"nvim-telescope/telescope.nvim",

	cmd = "Telescope",

	init = function()
		-- HACK: fixes folds for buffers opened via telescope
		vim.api.nvim_create_autocmd("BufEnter", {
			group = vim.api.nvim_create_augroup("Folds", { clear = true }),
			callback = function()
				vim.cmd.normal "zx"
			end,
		})

		---@diagnostic disable-next-line: duplicate-set-field
		vim.ui.select = function(...)
			require("lazy").load { plugins = { "telescope.nvim" } }
			vim.ui.select(...)
		end
	end,

	keys = {
		{
			"<C-p>",
			function()
				require("telescope.builtin").find_files()
			end,
			desc = "Find files",
		},
		{
			"<leader>o",
			function()
				require("telescope.builtin").oldfiles()
			end,
			desc = "Old files",
		},
		{
			"<leader>H",
			function()
				require("telescope.builtin").help_tags()
			end,
			desc = "Help tags",
		},
		{
			"<leader>/",
			function()
				require("telescope.builtin").live_grep()
			end,
			desc = "Live grep",
		},
		{
			"<leader>ba",
			function()
				require("telescope.builtin").buffers()
			end,
			desc = "Buffers",
		},
		{
			"<leader>bc",
			function()
				require("telescope.builtin").git_bcommits()
			end,
			desc = "Git BCommits",
		},
	},

	dependencies = {
		"nvim-lua/plenary.nvim",
		"nvim-telescope/telescope-ui-select.nvim",

		{
			"nvim-telescope/telescope-fzf-native.nvim",
			build = "make",
			enabled = vim.fn.executable "make" == 1,

			config = function()
				on_load("telescope.nvim", function()
					require("telescope").load_extension "fzf"
				end)
			end,
		},
	},

	config = function()
		local telescope = require "telescope"
		local actions = require "telescope.actions"

		telescope.setup {
			defaults = {
				path_display = { "truncate" },
				file_ignore_patterns = { ".git/", "target/", ".venv/" },

				multi_icon = " *",
				sorting_strategy = "ascending",
				layout_config = {
					prompt_position = "top",
					height = 30,
					width = 140,
				},

				mappings = {
					i = {
						["<C-s>"] = actions.select_horizontal,
						["<C-j>"] = actions.cycle_history_next,
						["<C-k>"] = actions.cycle_history_prev,
						["<C-y>"] = actions.select_default,
					},
				},
			},

			pickers = {
				find_files = {
					no_ignore = true,
					hidden = true,
				},
			},

			extensions = {
				fzf = {
					fuzzy = true,
					override_generic_sorter = true,
					override_file_sorter = true,
					case_mode = "smart_case",
				},
			},
		}

		require("telescope").load_extension "fzf"
		require("telescope").load_extension "ui-select"
	end,
}
