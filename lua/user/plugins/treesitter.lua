return {
	{
		"nvim-treesitter/nvim-treesitter",

		build = ":TSUpdate",
		event = { "BufReadPost", "BufNewFile", "VeryLazy" },

		dependencies = {
			"nvim-treesitter/nvim-treesitter-textobjects",
		},

		opts = {
			auto_install = true,
			sync_install = false,

			ensure_installed = {
				"c",
				"lua",
				"vim",
				"vimdoc",
				"query",
				"markdown",
				"markdown_inline",
				"rust",

				"gitcommit",
				"gitignore",
				"diff",
			},

			highlight = {
				enable = true,
				disable = { "vim", "latex", "tmux" }, -- does not work in the <c-f> buffer from cmdline
				-- disable = function(buf)
				-- 	local max_filesize = 100 * 1024 -- 100 KB
				-- 	local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))
				-- 	if ok and stats and stats.size > max_filesize then
				-- 		return true
				-- 	end
				-- end,

				additional_vim_regex_highlighting = { "latex" },
			},

			indent = {
				enable = true,
			},

			textobjects = {
				select = {
					enable = true,
					lookahead = true,
					include_surrounding_whitespace = false,

					keymaps = {
						["au"] = "@block.outer",
						["iu"] = "@block.inner",

						["ao"] = "@loop.outer",
						["io"] = "@loop.inner",

						["aa"] = "@parameter.outer",
						["ia"] = "@parameter.inner",

						["af"] = "@function.outer",
						["if"] = "@function.inner",

						["ac"] = "@class.outer",
						["ic"] = "@class.inner",
					},
				},

				move = {
					enable = true,
					set_jumps = true,

					goto_next_start = {
						["]a"] = "@parameter.inner",
						["]m"] = "@function.outer",
						["]h"] = "@class.outer",
					},

					goto_next_end = {
						["]M"] = "@function.outer",
						["]H"] = "@class.outer",
					},

					goto_previous_start = {
						["[a"] = "@parameter.inner",
						["[m"] = "@function.outer",
						["[h"] = "@class.outer",
					},

					goto_previous_end = {
						["[M"] = "@function.outer",
						["[H"] = "@class.outer",
					},
				},

				swap = {
					enable = true,

					swap_next = {
						["<leader>n"] = "@parameter.inner",
					},

					swap_previous = {
						["<leader>N"] = "@parameter.inner",
					},
				},
			},
		},

		config = function(_, opts)
			require("nvim-treesitter.configs").setup(opts)
		end,
	},

	{
		"nvim-treesitter/nvim-treesitter-context",
		event = { "BufReadPost", "BufNewFile" },

		dependencies = {
			"nvim-treesitter/nvim-treesitter",
		},

		opts = {
			max_lines = 5,
		},
	},
}
