local map = vim.keymap.set

-- Clipboard management
map({ "n", "v" }, "<leader>y", [["+y]])
map({ "n", "v" }, "<leader>Y", [["+y$]])
map({ "n", "v" }, "<leader>p", [["+p]])
map({ "n", "v" }, "<leader>P", [["+P]])
map({ "n", "v" }, "<leader>d", [["_d]])

-- Nicer keymaps for resizing splits
map("n", "<A-h>", "<cmd>vertical resize -2<CR>")
map("n", "<A-j>", "<cmd>resize +2<CR>")
map("n", "<A-k>", "<cmd>resize -2<CR>")
map("n", "<A-l>", "<cmd>vertical resize +2<CR>")

-- Nice for deleting snippet placeholders
map("s", "<C-h>", [[<C-g>"_c]])
map("s", "<BS>", [[<C-g>"_c]])

-- Make stuff centered
map("n", "J", "mzJ`z")
-- :h zv
map("n", "n", "nzzzv")
map("n", "N", "Nzzzv")
map("n", "<C-u>", "<C-u>zz")
map("n", "<C-d>", "<C-d>zz")
map("n", "G", "Gzz")

-- Switch thought buffers/quickfix list/location list
map("n", "]b", "<cmd>bnext<CR>")
map("n", "[b", "<cmd>bprevious<CR>")
map("n", "<C-j>", "<cmd>cnext<CR>zz")
map("n", "<C-k>", "<cmd>cprevious<CR>zz")
map("n", "<leader>j", "<cmd>lnext<CR>zz")
map("n", "<leader>k", "<cmd>lprev<CR>zz")

-- Move selection up/down
map("x", "<C-j>", ":m '>+1<CR>gv=gv")
map("x", "<C-k>", ":m '<-2<CR>gv=gv")

-- Diagnostics
map("n", "]d", vim.diagnostic.goto_next, { desc = "Go to next diagnostic" })
map("n", "[d", vim.diagnostic.goto_prev, { desc = "Go to previous diagnostic" })
map("n", "<leader>e", vim.diagnostic.open_float, { desc = "Open diagnostic in float" })
map("n", "<leader>E", vim.diagnostic.setqflist, { desc = "Set quickfix list with diagnostics" })

-- Usefull stuff
map("n", "<leader>S", "<cmd>set spell!<CR>", { desc = "Toggle spell" })
map("n", "<leader>i", vim.cmd.Inspect, { desc = "Inspect Pos" })
map("n", "<leader>I", vim.cmd.InspectTree, { desc = "Open Inspect Tree" })
map("n", "<leader>cd", "<cmd>cd %:h<CR>", { desc = "Change current directory" })
map("n", "<leader>s", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]], { desc = "Substitute word under cursor" })
map("n", "gp", "`[v`]", { desc = "Reselect pasted text." })
map("x", "Q", ":norm Q<CR>", { silent = true, desc = "Run last macro on the selection." })

-- Rename current file (not the grandest code but it works)
map("n", "<leader>r", function()
	local old_name = vim.fn.expand "%:t"
	local ok, new_name = pcall(vim.fn.input, "New file name: ", old_name)

	if ok == false or new_name == "" or new_name == old_name then
		print()
		return
	end

	vim.cmd.saveas("%:p:h/" .. new_name)
	vim.fn.delete(vim.fn.expand "#")
end, { desc = "Rename current file" })
