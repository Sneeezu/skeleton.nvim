# skeleton.nvim

Neovim configuration for beginners.

![showcase](docs/showcase.png)

## Installation

### Install these dependencies so that everything can function correctly:

-   neovim 0.10.0 (the newer, the better)
-   ripgrep (telescope live grep)

For lsps:

-   inotify-tools (better lsp performance, for more info :h inotify-limitations)
-   nodejs (js or ts based lsps)
-   python (python based lsps)

For lsp installation (Mason):

-   git
-   curl or wget
-   unzip
-   tar or gtar
-   gzip
-   npm
-   go (optional)

For TreeSitter:

-   git
-   nodejs
-   tree-sitter-cli
-   some c compiler

### Create backup of your old configuration:

```bash
# Required
mv ~/.config/nvim{,.bak}

# Recommended
mv ~/.cache/nvim{,.bak}
mv ~/.local/share/nvim{,.bak}
mv ~/.local/state/nvim{,.bak}
```

### Clone the repository:

```sh
git clone https://codeberg.org/Sneeezu/skeleton.nvim ~/.config/nvim
```

### Remove unnecessary files:

```sh
rm -rf ~/.config/nvim/docs
```

### Open Neovim:

```sh
nvim
```
